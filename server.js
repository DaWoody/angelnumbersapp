var express = require('express'),
    request = require('request'),
    app = express();

app.use(express.static('public'));
app.set('port', process.env.PORT || 4000);

app.get('/api', function(req, resp){
    var urlToFetch = req.query.url;

    request(urlToFetch, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            resp.send(body);
        }
    });
});

app.listen(app.get('port'), function(){
    console.log('Angular Number App started on ' + app.get('port'));
});

