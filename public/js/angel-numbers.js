/**
 *  Description: A small script I made to be able to parse and fetch the Angel Numbers from Sacred Scribes - All data and credit belongs to the author, Johanna Wemsley
 *  Author: Johan Wedfelt
 *  AuthorUrl: http://www.wedfelt.se
 *
 */
jQuery(document).ready(function(){
    var form = jQuery('.angel-number-form'),
        angelNumber = form.find('[data-input-type="angel-number"]'),
        outputWrapper = jQuery('.angel-number-output-wrapper'),
        errorText = "Sorry, it seems we couldn't yet fetch that peticular number :)",
        errorIndexText = "Error could not fetch the index numbers and store them in the DB...",
        angelNumbersMap = new Map(),
        repeatingNumbersMap = new Map();

    //The index url to the site, currently the implementation has been redirected to
    //Sacred scribes. This was not allowed by Joanne (Author of Sacred Scribes) so
    //the suggestion would be to redirect this path and adjust so the fetching goes towards
    //another site or if the user is allowed to get data from the Sacred Scribes website.
    var indexUrl = 'http://sacredscribesangelnumbers.blogspot.se/p/index-numbers.html',
        fallbackUrl = 'http://sacredscribesangelnumbers.blogspot.se/';


    //Repeating numbers, these could also be changed
    var number_1_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_25.html',
        number_2_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_2237.html',
        number_3_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_3889.html',
        number_4_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_9737.html',
        number_5_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_9001.html',
        number_6_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_1372.html',
        number_7_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_3615.html',
        number_8_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_3111.html',
        number_9_url = 'http://sacredscribesangelnumbers.blogspot.se/2010/08/angel-numbers-number-sequences_998.html';

    function getAngelNumber(number){

        var finalUrl = angelNumbersMap.has(number) ? angelNumbersMap.get(number) : getAlternativeUrl(number);

        jQuery.ajax({
            method:'get',
            url: '/api',
            data: {
                url: finalUrl
            },
            success: function(response){
                console.log('Success..');
                // console.log(response);

                var title = jQuery(response).find('.post-title'),
                    body = jQuery(response).find("[itemprop='description articleBody']");


                outputWrapper.html(body.html());
            },
            error: function(response){
                console.log(errorText);
                outputWrapper.html(errorText);
            }
        });
    }

    function isRepeatingNumberSequence(number){
        var isRepeatingNumber = false,
            numberAsString = number.toString(),
            numbersAsArray = numberAsString.split('');

        var firstNumber = 0;
        for(var i = 0, numberLength = numbersAsArray.length; i < numberLength; i++){
            if(i == 0){
                firstNumber = numbersAsArray[0];
            }

            if(firstNumber !== numbersAsArray[i]){
                return isRepeatingNumber;
            }
        }

        isRepeatingNumber = true;

        return isRepeatingNumber;
    }

    function getAngelNumberIndexes(){

        jQuery.ajax({
            method:'get',
            url: '/api',
            data: {
                url: indexUrl
            },
            success: function(response){
                console.log('Success..');

                var title = jQuery(response).find('.post-title'),
                    body = jQuery(response).find("[itemprop='description articleBody']"),
                    allLinks = body.find('a');

                allLinks.each(function(index, element){
                    var linkNumber = jQuery(this).text(),
                        link = jQuery(this).attr('href');

                    angelNumbersMap.set(linkNumber, link);
                });

                console.log('Stuff done.. :)');
                //outputWrapper.html(body.html());
            },
            error: function(response){
                console.log(errorIndexText);
                outputWrapper.html(errorIndexText);
            }
        })
    }

    function addRepeatingNumberSequences(){

        repeatingNumbersMap.set('1', number_1_url);
        repeatingNumbersMap.set('2', number_2_url);
        repeatingNumbersMap.set('3', number_3_url);
        repeatingNumbersMap.set('4', number_4_url);
        repeatingNumbersMap.set('5', number_5_url);
        repeatingNumbersMap.set('6', number_6_url);
        repeatingNumbersMap.set('7', number_7_url);
        repeatingNumbersMap.set('8', number_8_url);
        repeatingNumbersMap.set('9', number_9_url);
    }

    function getAlternativeUrl(number){
        //Lets first try and see if the numbers is a repeating numbers
        var isRepeatingNumber = isRepeatingNumberSequence(number),
            returnUrl = '';


        if(isRepeatingNumber){
            var firstInteger = number.toString().split('')[0];
            returnUrl = repeatingNumbersMap.get(firstInteger.toString());

        } else {
            try {
                var numberArray = number.split(''),
                    numberToFetch = 0;

                for(var j = 0, numberArrayLength = numberArray.length; j < numberArrayLength; j++){
                    if(j == 0){
                        numberToFetch = numberArray[j]
                    } else {
                        if(j < 3){
                            var currentNumberToFetchAsString = numberToFetch.toString(),
                                numberToAppendAsString = numberArray[j].toString(),
                                resultingNumberString = currentNumberToFetchAsString + numberToAppendAsString;
                            numberToFetch = parseInt(resultingNumberString, 10);
                        }
                    }
                }
                returnUrl = angelNumbersMap.get(numberToFetch.toString());
            } catch(e){
                returnUrl = fallbackUrl;
            }
        }

        return returnUrl;
    }

    //Add eventlistener
    form.submit(function(event){
        event.preventDefault();
        console.log('OK clicked');
        var number = angelNumber.val();
        getAngelNumber(number);
    });

    addRepeatingNumberSequences();
    getAngelNumberIndexes();

});