jQuery(document).ready(function(){


      var form = jQuery('.node-test-form'),
          apiPath = form.find('[data-input-type="api-path"]'),
          postTypes = form.find('[data-input-type="post-type"]'),
          headerFields = form.find('.extra-form-values-headers').find('.node-test-data-to-send-wrapper'),
          dataFields = form.find('.extra-form-values-data').find('.node-test-data-to-send-wrapper'),
          outPutWrapper = jQuery('.output-wrapper');

      var serverUrl = 'http://localhost',
          serverPort = 3000;

      function returnPostType(){

        var postType = 'post';

        jQuery.each(postTypes, function(index, element){

          if(element.checked){
            postType = element.value;
          }
          console.log('This is the postType thing..' + element.value);
        });

        return postType;
      }

      function returnApiPath(){
        var apiPathValue = apiPath.val() || '',
            returnPath = serverUrl  + ':' + serverPort + '/' + apiPathValue;

        return returnPath;
      }

      function getAllValuesAsObject(fields){

          var returnData = {};

        jQuery.each(fields, function(index, element){
                //Now we have a value..

            var fieldName = jQuery(element).find('[data-input-type="data-input-name"]').val(),
                fieldValue = jQuery(element).find('[data-input-type="data-input-value"]').val();

            if(fieldName !== ''){
                returnData[fieldName] = fieldValue
            }
        });
          return returnData;
      }

      form.submit(function(event){
        event.preventDefault();

        console.log('Full server url.. is' + returnApiPath());

          var postType = returnPostType(),
              apiPath = returnApiPath(),
              dataObject = getAllValuesAsObject(dataFields),
              headerObject = getAllValuesAsObject(headerFields);

          console.log('The path is..' + apiPath);
          console.log('The post type is..' + postType);

          console.log('The data object..');
          console.log(dataObject);

          console.log('The header object..');
          console.log(headerObject);

          jQuery.ajax({
              contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
              dataType: 'json',
              data: dataObject,
              headers: headerObject,
              method: postType,
              url: apiPath,
              beforeSend: function(){
                console.log('Did we try this before sending');
              },
              success: function(response){
                  console.log('Thos is the success response..');
                  console.log(response);

                  console.log(outPutWrapper);
                  var jsonObject = JSON.stringify(response);

                  outPutWrapper.text(jsonObject);

              },
              error: function(response){
                  console.log('error recieved..');
                  console.log(response);
              }
          });

      });




});