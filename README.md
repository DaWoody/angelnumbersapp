#Angel Numbers
***

#### * A small single page application that fetches angel numbers from the site [Angel Numbers](http://sacredscribesangelnumbers.blogspot.se/).

###About

Technology wise the application is a small [jQuery](https://jquery.com/) based application which utilizes a [Node](https://nodejs.org/en/)  ```express``` server
with an internal proxy (so the [CORS](https://en.wikipedia.org/wiki/Cross-origin_resource_sharing) situation is circumvented)

This application was intented as a [Singel Page Application](https://en.wikipedia.org/wiki/Single-page_application) fetching angel numbers from the 
blog [Angel Numbers - Sacred Scribes](http://sacredscribesangelnumbers.blogspot.se/). According to the Author of the site and my personal interpretation of the possibility of sharing the 
messages provided in the numbers if it was for non-profit and credit was given back to the site and author, I created this application
as a simpler means to fetch the numbers without sometimes googling the specific number (to get the correct link to the blog)
or going through the index page on the blog.

This is the statement provided by the author on the site


> ANGEL NUMBERS - A Guide to Repeating Number Sequences and their Messages and Meanings. 
> You are most welcome to share these Angel Number messages with others, although I do request that you include this website address, 
> credit your source/page links and author. All postings by Joanne Walmsley - Sacred Scribes may be used for personal, not-for-profit purposes only. 
> sacredscribes@gmail.com Joanne Walmsley Sacred Scribes Australia

Of course I also asked the author for permission to post this site/application online as complement to the existing site (traffic would still be proxied to the site with hits and views),
but unfortunately the author did not want this application to be up and running, so therefor the application is not online anymore, but
I thought I would share the code on how I did create the solution and application since the technology might be interesting for other purposes.

Cheers!

/Johan DaWoody Wedfelt

### How to
Since the application runs on a [Node](https://nodejs.org/en/) platform, ```npm install``` and then run ```npm start``` will start the application
@ *your_server_url*:4000 (most likely *your_server_url* will be http://localhost if you are not using cloud based platform like for instance [Heroku](https://www.heroku.com/)),
